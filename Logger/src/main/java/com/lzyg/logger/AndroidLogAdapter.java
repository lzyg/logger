package com.lzyg.logger;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Android terminal log output implementation for {@link LogAdapter}.
 * <p>
 * Prints output to LogCat with pretty borders.
 *
 * <pre>
 *  ┌──────────────────────────
 *  │ Method stack history
 *  ├┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄
 *  │ Log message
 *  └──────────────────────────
 * </pre>
 *
 * @author zhengyunguang
 * @date 2018-10-13 18:07:28
 */
public class AndroidLogAdapter implements LogAdapter {

    @NonNull
    private FormatStrategy formatStrategy;
    @NonNull
    private JLogConfig config;

    public AndroidLogAdapter(JLogConfig config) {
        this.config = config;
        formatStrategy = AndroidLogStrategy.newBuilder()
                .showThreadInfo(config.showThreadInfo())
                .methodCount(config.methodCount())
                .tag(config.tag())
                .methodOffset(config.methodOffset())
                .build();
    }

    @Override
    public boolean isLoggable() {
        return config.debug();
    }

    @Override
    public boolean isLoggable(int priority, @Nullable String tag) {
        return config.debug();
    }

    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        formatStrategy.log(priority, tag, message);
    }

}