package com.lzyg.logger;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import static com.lzyg.logger.Utils.checkNotNull;


/**
 * This is used to saves log messages to the disk.
 *
 * @author zhengyunguang
 * @date 2018-10-13 18:51:45
 */
public class DiskLogAdapter implements LogAdapter {

    @NonNull
    private FormatStrategy formatStrategy;
    @NonNull
    private JLogConfig config;


    public DiskLogAdapter(@NonNull JLogConfig config) {
        this.config = config;
        formatStrategy = TxtFormatStrategy.newBuilder()
                .showThreadInfo(config.showThreadInfo())
                .withCaller(config.methodCount() > 0)
                .tag(config.tag())
                .build();
    }


    public DiskLogAdapter(@NonNull FormatStrategy formatStrategy) {
        this.formatStrategy = checkNotNull(formatStrategy);
    }

    @Override
    public boolean isLoggable() {
        return config.toFile();
    }

    @Override
    public boolean isLoggable(int priority, @Nullable String tag) {
        return config.toFile();
    }

    @Override
    public void log(int priority, @Nullable String tag, @NonNull String message) {
        formatStrategy.log(priority, tag, message);
    }
}
