package com.lzyg.logger;

import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;

import static com.lzyg.logger.Utils.DEFAULT_FOLDER_NAME;
import static com.lzyg.logger.Utils.checkNotNull;


/**
 * JLog简介
 * 为了修改一些流程，copy from com.orhanobut.logger.Logger
 *
 * @author orhanobut
 * @date 2018-10-13 17:12:15
 */
public class JLog {


    /**
     * 输出Error信息
     *
     * @param msg 异常信息
     */
    public static void e(String msg) {
        e(null, msg);
    }

    /**
     * 输出Error信息
     *
     * @param tag 异常信息标识
     * @param msg 异常信息
     */
    public static void e(String tag, String msg) {
        if (msg == null) {
            return;
        }
        t(tag).e(msg);
    }

    /**
     * 输出警告信息
     *
     * @param msg 错误信息
     */
    public static void w(String msg) {
        w(null, msg);
    }

    /**
     * 输出警告信息
     *
     * @param tag 错误信息标识
     * @param msg 错误信息
     */
    public static void w(String tag, String msg) {
        if (msg == null) {
            return;
        }
        t(tag).w(msg);
    }

    /**
     * 输出普通信息
     *
     * @param msg 异常信息
     */
    public static void i(String msg) {
        i(null, msg);
    }

    /**
     * 输出普通信息
     *
     * @param tag 普通信息
     * @param msg 异常信息
     */
    public static void i(String tag, String msg) {
        if (msg == null) {
            return;
        }
        t(tag).i(msg);
    }

    /**
     * 输出debug信息
     *
     * @param msg 调试信息
     */
    public static void d(String msg) {
        d(null, msg);
    }

    /**
     * 输出debug信息
     *
     * @param tag 调试信息标识
     * @param msg 调试信息
     */
    public static void d(String tag, String msg) {
        if (msg == null) {
            return;
        }
        t(tag).d(msg);
    }

    /**
     * 输出VERBOSE信息
     *
     * @param msg 调试信息
     */
    public static void v(String msg) {
        v(null, msg);
    }

    /**
     * 输出VERBOSE信息
     *
     * @param tag 调试信息标识
     * @param msg 调试信息
     */
    public static void v(String tag, String msg) {
        if (msg == null) {
            return;
        }
        t(tag).v(msg);
    }

    /**
     * Given tag will be used as tag only once for this method call regardless of the tag that's been
     * set during initialization. After this invocation, the general tag that's been set will
     * be used for the subsequent log calls
     */
    public static Printer t(@Nullable String tag) {
        return printer.t(tag);
    }

    /**
     * Tip: Use this for exceptional situations to log
     * ie: Unexpected errors etc
     */
    public static void wtf(@NonNull String message, @Nullable Object... args) {
        if (message == null) {
            return;
        }
        printer.wtf(message, args);
    }

    /**
     * Formats the given json content and print it
     */
    public static void json(@Nullable String json) {
        if (json == null) {
            return;
        }
        printer.json(json);
    }

    /**
     * Formats the given xml content and print it
     */
    public static void xml(@Nullable String xml) {
        if (xml == null) {
            return;
        }
        printer.xml(xml);
    }


    /**
     * printer
     */
    @NonNull
    private static Printer printer = new JLogPrinter();


    /**
     * init
     *
     * @param config
     */
    public static void init(@NonNull JLogConfig config) {
        //add logcat log adapter
        addLogAdapter(new AndroidLogAdapter(config));
        //add disk log adapter
        DiskLogAdapter localAdapter = new DiskLogAdapter(config);
        addLogAdapter(localAdapter);
    }

    public static boolean isLoggable() {
        return printer.isLoggable();
    }

    /**
     * set printer
     *
     * @param printer
     */
    public static void printer(@NonNull Printer printer) {
        JLog.printer = checkNotNull(printer);
    }

    /**
     * add
     *
     * @param adapter
     */
    public static void addLogAdapter(@NonNull LogAdapter adapter) {
        printer.addAdapter(checkNotNull(adapter));
    }

    /**
     * clear
     */
    public static void clearLogAdapters() {
        printer.clearLogAdapters();
    }


    public static void clearLogFiles(final Handler handler) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    String diskPath = Environment.getExternalStorageDirectory().getAbsolutePath();
                    String folder = diskPath + File.separatorChar + DEFAULT_FOLDER_NAME;
                    File file = new File(folder);
                    if (file.exists()) {
                        File[] logFiles = file.listFiles();
                        if (logFiles != null && file.length() > 0) {
                            for (File logFile : logFiles) {
                                logFile.delete();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (handler != null) {
                    handler.sendEmptyMessage(0);
                }

            }
        };
        thread.start();
    }


}
