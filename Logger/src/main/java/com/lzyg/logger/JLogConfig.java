package com.lzyg.logger;

/**
 * JLogConfig简介
 *
 * @author zhengyunguang
 * @date 2018-10-13 18:03:49
 */
public class JLogConfig {

    /**
     * 是否打印到logcat
     */
    private boolean debug;

    /**
     * 是否输出到文件
     */
    private boolean toFile;

    /**
     * showThreadInfo 是否显示线程信息
     */
    private boolean showThreadInfo;
    /**
     * methodCount 显示调用方法的个数，默认为3，只对Logcat有效，写文件默认只显示一个
     */
    private int methodCount = 3;
    /**
     * methodOffset 调用方法偏移，JLog被包装时偏移调用层次
     */
    private int methodOffset = 0;
    /**
     * tag 全局Tag标签
     */
    private String tag;

    private JLogConfig(Builder builder) {
        this.debug = builder.debug;
        this.toFile = builder.toFile;
        this.showThreadInfo = builder.showThreadInfo;
        this.methodCount = builder.methodCount;
        this.methodOffset = builder.methodOffset;
        this.tag = builder.tag;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public boolean debug() {
        return debug;
    }

    public boolean toFile() {
        return toFile;
    }

    public boolean showThreadInfo() {
        return showThreadInfo;
    }

    public int methodCount() {
        return this.methodCount;
    }

    public int methodOffset() {
        return this.methodOffset;
    }

    public String tag() {
        return this.tag;
    }


    public static class Builder {

        /**
         * 是否打印到logcat
         */
        private boolean debug;
        /**
         * 是否输出到文件
         */
        private boolean toFile;
        /**
         * showThreadInfo 是否显示线程信息
         */
        private boolean showThreadInfo;
        /**
         * methodCount 显示调用方法的个数，默认为3，只对Logcat有效，写文件默认只显示一个
         */
        private int methodCount = 3;
        /**
         * methodOffset 调用方法偏移，JLog被包装时偏移调用层次
         */
        private int methodOffset = 0;
        /**
         * tag 全局Tag标签
         */
        private String tag;

        private Builder() {
        }

        public Builder debug(boolean debug) {
            this.debug = debug;
            return this;
        }

        public Builder toFile(boolean toFile) {
            this.toFile = toFile;
            return this;
        }

        public Builder showThreadInfo(boolean showThreadInfo) {
            this.showThreadInfo = showThreadInfo;
            return this;
        }


        public Builder methodCount(int methodCount) {
            this.methodCount = methodCount;
            return this;
        }

        public Builder methodOffset(int methodOffset) {
            this.methodOffset = methodOffset;
            return this;
        }

        public Builder tag(String tag) {
            this.tag = tag;
            return this;
        }

        public JLogConfig build() {
            return new JLogConfig(this);
        }
    }
}
