package com.jd.zyg;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.jd.zyg.test.R;
import com.lzyg.logger.JLog;
import com.lzyg.logger.JLogConfig;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        JLog.init(JLogConfig.newBuilder().debug(true)/*.tag("JLog")*/.showThreadInfo(true).toFile(true).build());
        TextView btnTest = findViewById(R.id.btnTest);
        btnTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JLog.d("123");
                JLog.d("MainAcitivity","123");
            }
        });
    }
}
